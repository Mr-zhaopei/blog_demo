package routers

import (
	"log"

	v1 "gitee.com/Mr-zhaopei/blog_demo/api/v1"
	"gitee.com/Mr-zhaopei/blog_demo/config"
	"gitee.com/Mr-zhaopei/blog_demo/middleware"
	"github.com/gin-gonic/gin"
)

func InitRouter() {
	var server config.Server
	s1 := server.Load("conf/app.ini").Init()

	log.Println(s1)
	gin.SetMode(server.AppMode)
	engin := gin.New()
	engin.Use(middleware.Logger(), middleware.Cors())
	engin.Use(gin.Recovery())

	//静态资源托管
	engin.LoadHTMLGlob("static/admin/index.html")

	engin.Static("admin/static", "static/admin/static")
	engin.StaticFile("admin/favicon.ico","static/admin/favicon.ico")

	engin.GET("/", func(ctx *gin.Context) {
		ctx.HTML(200, "index.html", nil)
	})

	auth := engin.Group("api/v1")
	auth.Use(middleware.JwtToken())
	{
		//用户模块的路由接口
		// auth.POST("user/add", v1.AddUser)
		// auth.GET("users", v1.GetUsers)
		auth.PUT("user/:id", v1.EditUser)
		auth.DELETE("user/:id", v1.DeleteUser)
		//修改密码
		auth.PUT("admin/changepw/:id", v1.ChangeUserPassword)
		//分类模块的路由接口
		auth.POST("category/add", v1.AddCategory)
		// router.GET("category", v1.GetCategorys)
		auth.PUT("category/:id", v1.EditCategory)
		auth.DELETE("category/:id", v1.DeleteCategory)
		//文章模块的路由接口
		auth.POST("article/add", v1.AddArticle)
		// router.GET("article", v1.GetArticles)
		// router.GET("article/list/:id", v1.GetCateArt)
		// router.GET("article/info/:id", v1.GetArticleInfo)
		auth.PUT("article/:id", v1.EditArticle)
		auth.DELETE("article/:id", v1.DeleteArticle)
		//上传文件
		auth.POST("upload", v1.UploadFile)
	}

	router := engin.Group("api/v1")
	{
		router.POST("user/add", v1.AddUser)
		router.GET("users", v1.GetUsers)
		router.GET("user/:id", v1.GetUserInfo)
		router.GET("category", v1.GetCategorys)
		router.GET("category/:id", v1.GetCateInfo)
		router.GET("article", v1.GetArticles)
		router.GET("article/list/:id", v1.GetCateArt)
		router.GET("article/info/:id", v1.GetArticleInfo)
		router.POST("login", v1.Login)
	}

	engin.Run(s1.Address + ":" + s1.Port)
}
