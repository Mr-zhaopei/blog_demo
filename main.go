package main

import (
	"gitee.com/Mr-zhaopei/blog_demo/model"
	"gitee.com/Mr-zhaopei/blog_demo/routers"
)

func main() {
	//引用数据库类型
	model.InitDb()
	routers.InitRouter()
}
