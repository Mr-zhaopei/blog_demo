package config_test

import (
	"fmt"
	"testing"

	"gitee.com/Mr-zhaopei/blog_demo/config"
)

func TestDB(t *testing.T) {
	var mysql config.Mysql
	db := mysql.Load("../conf/db.ini").Init()
	fmt.Println(db)
	if db == nil {
		t.Fail()
	}
}

func TestServer(t *testing.T){
	var server config.Server
	s1 := server.Load("../conf/app.ini").Init()
	fmt.Println(s1)
}
