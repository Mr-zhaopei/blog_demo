package config

import (
	"gitee.com/Mr-zhaopei/blog_demo/utils"
	"github.com/go-ini/ini"
)

// var Server *server

type Server struct {
	AppMode string
	Address string
	Port    string
	JwtKey  string
	source  *ini.File
	LogFile string

	AccessKey   string
	Secretkey   string
	Bucket      string
	QiniuServer string
	Zone        string
}

func (s *Server) Load(path string) *Server {
	var err error
	//1.判断配置文件是否存在
	exists, _ := utils.PathExists(path)
	if !exists {
		return s
	}
	s.source, err = ini.Load(path)
	if err != nil {
		panic(err)
	}
	return s
}

func (s *Server) Init() *Server {
	//判断配置文件是否加载成功
	if s.source == nil {
		return s
	}
	s.AppMode = s.source.Section("server").Key("AppMode").MustString("debug")
	s.Address = s.source.Section("server").Key("address").MustString("0.0.0.0")
	s.Port = s.source.Section("server").Key("port").MustString("8080")
	s.JwtKey = s.source.Section("server").Key("JwtKey").MustString("ginblog.com")
	s.LogFile = s.source.Section("server").Key("LogFile").MustString("/var/log/ginblog.log")

	s.AccessKey = s.source.Section("storage").Key("AccessKey").String()
	s.Secretkey = s.source.Section("storage").Key("Secretkey").String()
	s.Bucket = s.source.Section("storage").Key("Bucket").String()
	s.QiniuServer = s.source.Section("storage").Key("QiniuServer").String()
	s.Zone = s.source.Section("storage").Key("Zone").String()

	return s
}
