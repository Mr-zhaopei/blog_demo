package config

import (
	"gitee.com/Mr-zhaopei/blog_demo/utils"
	"github.com/go-ini/ini"
)

type Mysql struct {
	Host     string `ini:"host"`
	Port     string `ini:"port"`
	Database string `ini:"database"`
	Username string `ini:"username"`
	Password string `ini:"password"`
	source   *ini.File
}

func (m *Mysql) Load(path string) *Mysql {
	var err error
	exists, _ := utils.PathExists(path)
	if !exists {
		return m
	}
	m.source, err = ini.Load(path)
	if err != nil {
		panic(err)
	}
	return m
}

func (m *Mysql) Init() *Mysql {
	//判断配置是否加载成功
	if m.source == nil {
		return m
	}
	//这里直接把配置映射到结构体
	err := m.source.Section("mysql").MapTo(m)
	if err != nil {
		panic(err)
	}
	return m
}
