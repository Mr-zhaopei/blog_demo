package middleware

import (
	"errors"
	"net/http"
	"strings"

	"gitee.com/Mr-zhaopei/blog_demo/config"
	"gitee.com/Mr-zhaopei/blog_demo/utils/errmsg"
	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
)

type JWT struct {
	JwtKey []byte
}

type MyClaims struct {
	Username string `json:"username"`
	jwt.StandardClaims
}

var (
	TokenExpired     = errors.New("Token is expired")
	TokenNotValidYet = errors.New("Token not active yet")
	TokenMalformed   = errors.New("That's not even a token")
	TokenInvalid     = errors.New("Couldn't handle this token:")
)

func NewJWT() *JWT {
	var server config.Server
	server.Load("conf/app.ini").Init()
	return &JWT{
		[]byte(server.JwtKey),
	}
}

// 生成token
// func (j *JWT) CreateToken(username string, password string) (string, int) {
// 	expireTime := time.Now().Add(10 * time.Hour)
// 	SetClaims := MyClaims{
// 		Username: username,
// 		Password: password,
// 		StandardClaims: jwt.StandardClaims{
// 			ExpiresAt: expireTime.Unix(),
// 			Issuer:    "ginblog",
// 		},
// 	}

// 	reqClaims := jwt.NewWithClaims(jwt.SigningMethodES256, SetClaims)
// 	token, err := reqClaims.SignedString(j.JwtKey)
// 	if err != nil {
// 		return "", errmsg.ERROR
// 	}
// 	return token, errmsg.SUCCESS
// }

// CreateToken 生成token
func (j *JWT) CreateToken(claims MyClaims) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	return token.SignedString(j.JwtKey)
}

// ParserToken 解析token
func (j *JWT) ParserToken(tokenString string) (*MyClaims, error) {
	token, err := jwt.ParseWithClaims(tokenString, &MyClaims{}, func(token *jwt.Token) (interface{}, error) {
		return j.JwtKey, nil
	})

	if err != nil {
		if ve, ok := err.(*jwt.ValidationError); ok {
			if ve.Errors&jwt.ValidationErrorMalformed != 0 {
				return nil, TokenMalformed
			} else if ve.Errors&jwt.ValidationErrorExpired != 0 {
				return nil, TokenExpired
			} else if ve.Errors&jwt.ValidationErrorNotValidYet != 0 {
				return nil, TokenNotValidYet
			} else {
				return nil, TokenInvalid
			}
		}
	}

	if token != nil {
		if claims, ok := token.Claims.(*MyClaims); ok && token.Valid {
			return claims, nil
		}
		return nil, TokenInvalid
	}

	return nil, TokenInvalid
}

// JwtToken jwt中间件
func JwtToken() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		var code int
		tokenHeader := ctx.Request.Header.Get("Authorization")
		if tokenHeader == "" {
			code = errmsg.ERROR_TOKEN_NOT_EXIST
			ctx.JSON(http.StatusOK, gin.H{
				"status":  code,
				"message": errmsg.GetErrMsg(code),
			})
			ctx.Abort()
			return
		}

		checkToken := strings.Split(tokenHeader, " ")

		if len(checkToken) == 0 {
			ctx.JSON(http.StatusOK, gin.H{
				"status":  code,
				"message": errmsg.GetErrMsg(code),
			})
			ctx.Abort()
			return
		}

		if len(checkToken) != 2 || checkToken[0] != "Bearer" {
			code = errmsg.ERROR_TOKEN_TYPE_WRONG
			ctx.JSON(http.StatusOK, gin.H{
				"status":  code,
				"message": errmsg.GetErrMsg(code),
			})
			ctx.Abort()
			return
		}

		j := NewJWT()

		// 解析token
		claims, err := j.ParserToken(checkToken[1])
		if err != nil {
			if err == TokenExpired {
				ctx.JSON(http.StatusOK, gin.H{
					"status":  errmsg.ERROR,
					"message": "token授权已过期,请重新登录",
					"data":    nil,
				})
				ctx.Abort()
				return
			}
			// 其他错误
			ctx.JSON(http.StatusOK, gin.H{
				"status":  errmsg.ERROR,
				"message": err.Error(),
				"data":    nil,
			})
			ctx.Abort()
			return
		}

		ctx.Set("username", claims)
		ctx.Next()
	}
}
