package middleware

import (
	"fmt"
	"math"
	"os"
	"time"

	"gitee.com/Mr-zhaopei/blog_demo/config"
	"github.com/gin-gonic/gin"
	retalog "github.com/lestrrat-go/file-rotatelogs"
	"github.com/rifflock/lfshook"
	"github.com/sirupsen/logrus"
)

// 自定义日志中间件
func Logger() gin.HandlerFunc {
	var logfile config.Server
	cfg := logfile.Load("conf/app.ini").Init()

	file, err := os.OpenFile(cfg.LogFile, os.O_RDWR|os.O_CREATE, 0755)
	if err != nil {
		fmt.Println("log file open error: ", err)
	}
	logger := logrus.New()
	logger.Out = file
	logger.SetLevel(logrus.DebugLevel)

	// linkName := "latest_log.log"
	//日志切割
	logWriter, _ := retalog.New(
		cfg.LogFile+"%Y%m%d.log",
		retalog.WithMaxAge(7*24*time.Hour),
		retalog.WithRotationTime(24*time.Hour),
		// retalog.WithLinkName(linkName),
	)

	writeMap := lfshook.WriterMap{
		logrus.InfoLevel:  logWriter,
		logrus.FatalLevel: logWriter,
		logrus.DebugLevel: logWriter,
		logrus.WarnLevel:  logWriter,
		logrus.ErrorLevel: logWriter,
		logrus.PanicLevel: logWriter,
	}

	Hook := lfshook.NewHook(writeMap, &logrus.TextFormatter{
		TimestampFormat: "2006-01-02 15:04:05",
	})

	logger.AddHook(Hook)

	return func(ctx *gin.Context) {
		startTime := time.Now()
		ctx.Next()
		stopTime := time.Since(startTime)
		//开销时间：向上取整
		spendTime := fmt.Sprintf("%d ms", int(math.Ceil(float64(stopTime.Nanoseconds())/1000000.0)))
		//主机名
		hostName, err := os.Hostname()
		if err != nil {
			hostName = "unknow"
		}

		//请求状态码
		statusCode := ctx.Writer.Status()
		//客户端ip
		clientip := ctx.ClientIP()
		userAgent := ctx.Request.UserAgent()
		dataSize := ctx.Writer.Size()
		if dataSize < 0 {
			dataSize = 0
		}
		method := ctx.Request.Method
		path := ctx.Request.RequestURI

		entry := logger.WithFields(logrus.Fields{
			"HostName":  hostName,
			"status":    statusCode,
			"SpendTime": spendTime,
			"IP":        clientip,
			"Method":    method,
			"Path":      path,
			"DataSize":  dataSize,
			"Agent":     userAgent,
		})

		//如果gin框架系统内部存在错误
		if len(ctx.Errors) > 0 {
			entry.Error(ctx.Errors.ByType(gin.ErrorTypePrivate).String())
		}

		if statusCode >= 500 {
			entry.Error()
		} else if statusCode >= 400 {
			entry.Warn()
		} else {
			entry.Info()
		}
	}
}
