package v1

import (
	"net/http"
	"strconv"

	"gitee.com/Mr-zhaopei/blog_demo/model"
	"gitee.com/Mr-zhaopei/blog_demo/utils/errmsg"
	"gitee.com/Mr-zhaopei/blog_demo/utils/validator"
	"github.com/gin-gonic/gin"
)

var (
	code int
)

// // 查询用户是否存在
// func UserExist(ctx *gin.Context) {}

// 添加用户
func AddUser(ctx *gin.Context) {
	var data model.User
	_ = ctx.ShouldBindJSON(&data)

	//校验data数据
	// fmt.Println(data)
	msg, code := validator.Validate(&data)
	if code != errmsg.SUCCESS {
		ctx.JSON(http.StatusOK, gin.H{
			"status": code,
			"data":   msg,
		})
		return
	}

	code = model.CheckUser(data.Username)
	if code == errmsg.SUCCESS {
		model.CreateUser(&data)
	}
	if code == errmsg.ERROR_USERNAME_USED {
		code = errmsg.ERROR_USERNAME_USED
	}
	ctx.JSON(http.StatusOK, gin.H{
		"status":  code,
		"data":    data,
		"message": errmsg.GetErrMsg(code),
	})
}

// 查询单个用户
func GetUserInfo(ctx *gin.Context) {
	id, _ := strconv.Atoi(ctx.Param("id"))
	data, code := model.GetUser(id)
	ctx.JSON(http.StatusOK, gin.H{
		"status":  code,
		"data":    data,
		"total":   1,
		"message": errmsg.GetErrMsg(code),
	})
}

// 查询用户列表
func GetUsers(ctx *gin.Context) {
	pageSize, _ := strconv.Atoi(ctx.Query("pagesize"))
	pageNum, _ := strconv.Atoi(ctx.Query("pagenum"))
	username := ctx.Query("username")
	if pageSize == 0 {
		pageSize = -1
	}
	if pageNum == 0 {
		pageNum = -1
	}
	users, total := model.GetUsers(username, pageSize, pageNum)
	code = errmsg.SUCCESS
	ctx.JSON(http.StatusOK, gin.H{
		"status":  code,
		"data":    users,
		"total":   total,
		"message": errmsg.GetErrMsg(code),
	})

}

// 编辑用户
func EditUser(ctx *gin.Context) {
	var data model.User
	_ = ctx.ShouldBindJSON(&data)
	id, _ := strconv.Atoi(ctx.Param("id"))
	code = model.CheckUpUser(id, data.Username)
	if code == errmsg.SUCCESS {
		model.EditUser(id, &data)
	}
	if code == errmsg.ERROR_USERNAME_USED {
		ctx.Abort()
	}
	ctx.JSON(http.StatusOK, gin.H{
		"status": code,
		// "data":    data,
		"message": errmsg.GetErrMsg(code),
	})
}

// ChangeUserPassword 修改密码
func ChangeUserPassword(ctx *gin.Context) {
	var data model.User
	_ = ctx.ShouldBindJSON(&data)
	id, _ := strconv.Atoi(ctx.Param("id"))
	code = model.CheckUpUser(id, data.Username)
	if code == errmsg.SUCCESS {
		model.ChangePassword(id,&data)
	}
	if code == errmsg.ERROR_USERNAME_USED {
		ctx.Abort()
	}
	ctx.JSON(
		http.StatusOK, gin.H{
			"status":  code,
			"message": errmsg.GetErrMsg(code),
		},
	)
}

// 删除用户
func DeleteUser(ctx *gin.Context) {
	id, _ := strconv.Atoi(ctx.Param("id"))
	code = model.DeleteUser(id)
	ctx.JSON(http.StatusOK, gin.H{
		"status":  code,
		"message": errmsg.GetErrMsg(code),
	})
}
