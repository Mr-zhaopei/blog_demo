package v1

import (
	"net/http"
	"time"

	"gitee.com/Mr-zhaopei/blog_demo/middleware"
	"gitee.com/Mr-zhaopei/blog_demo/model"
	"gitee.com/Mr-zhaopei/blog_demo/utils/errmsg"
	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
)

func Login(ctx *gin.Context) {
	var data model.User
	var token string
	ctx.ShouldBind(&data)
	code := model.CheckLogin(data.Username, data.Password)

	//token生成
	if code == errmsg.SUCCESS {
		setToken(ctx, data)
	} else {
		ctx.JSON(http.StatusOK, gin.H{
			"status":  code,
			"data":    data.Username,
			"id":      data.ID,
			"message": errmsg.GetErrMsg(code),
			"token":   token,
		})
	}

}

// token生成函数
func setToken(c *gin.Context, user model.User) {
	j := middleware.NewJWT()
	claims := middleware.MyClaims{
		Username: user.Username,
		StandardClaims: jwt.StandardClaims{
			NotBefore: time.Now().Unix() - 100,
			ExpiresAt: time.Now().Unix() + 604800,
			Issuer:    "GinBlog",
		},
	}

	token, err := j.CreateToken(claims)

	if err != nil {
		c.JSON(http.StatusOK, gin.H{
			"status":  errmsg.ERROR,
			"message": errmsg.GetErrMsg(errmsg.ERROR),
			"token":   token,
		})
	}

	c.JSON(http.StatusOK, gin.H{
		"status":  200,
		"data":    user.Username,
		"id":      user.ID,
		"message": errmsg.GetErrMsg(200),
		"token":   token,
	})
	return
}
