package v1

import (
	"net/http"

	"gitee.com/Mr-zhaopei/blog_demo/model"
	"gitee.com/Mr-zhaopei/blog_demo/utils/errmsg"
	"github.com/gin-gonic/gin"
)

func UploadFile(ctx *gin.Context) {
	file, fileHeader, _ := ctx.Request.FormFile("file")
	filesize := fileHeader.Size

	url, code := model.UploadFile(file, filesize)
	ctx.JSON(http.StatusOK, gin.H{
		"status":  code,
		"message": errmsg.GetErrMsg(code),
		"url":     url,
	})
}
