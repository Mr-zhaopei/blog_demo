package v1

import (
	"net/http"
	"strconv"

	"gitee.com/Mr-zhaopei/blog_demo/model"
	"gitee.com/Mr-zhaopei/blog_demo/utils/errmsg"
	"github.com/gin-gonic/gin"
)

// 添加分类
func AddCategory(ctx *gin.Context) {
	var data model.Category
	_ = ctx.ShouldBindJSON(&data)
	code = model.CheckCategory(data.Name)
	if code == errmsg.SUCCESS {
		model.CreateCategory(&data)
	}
	if code == errmsg.ERROR_CATEGORYNAME_USED {
		code = errmsg.ERROR_CATEGORYNAME_USED
	}
	ctx.JSON(http.StatusOK, gin.H{
		"status":  code,
		"data":    data,
		"message": errmsg.GetErrMsg(code),
	})
}

// 查询单个分类
func GetCateInfo(ctx *gin.Context) {
	id, _ := strconv.Atoi(ctx.Param("id"))
	cate, code := model.GetCateInfo(id)
	ctx.JSON(http.StatusOK, gin.H{
		"status":  code,
		"data":    cate,
		"message": errmsg.GetErrMsg(code),
	})
}

// 查询分类列表
func GetCategorys(ctx *gin.Context) {
	pageSize, _ := strconv.Atoi(ctx.Query("pagesize"))
	pageNum, _ := strconv.Atoi(ctx.Query("pagenum"))
	if pageSize == 0 {
		pageSize = -1
	}
	if pageNum == 0 {
		pageNum = -1
	}
	Categorys, total := model.GetCategorys(pageSize, pageNum)
	code = errmsg.SUCCESS
	ctx.JSON(http.StatusOK, gin.H{
		"status":  code,
		"data":    Categorys,
		"total":   total,
		"message": errmsg.GetErrMsg(code),
	})

}

// 编辑分类名称
func EditCategory(ctx *gin.Context) {
	var data model.Category
	_ = ctx.ShouldBindJSON(&data)
	id, _ := strconv.Atoi(ctx.Param("id"))
	code = model.CheckCategory(data.Name)
	if code == errmsg.SUCCESS {
		model.EditCategory(id, &data)
	}
	if code == errmsg.ERROR_CATEGORYNAME_USED {
		ctx.Abort()
	}
	ctx.JSON(http.StatusOK, gin.H{
		"status": code,
		// "data":    data,
		"message": errmsg.GetErrMsg(code),
	})
}

// 删除分类
func DeleteCategory(ctx *gin.Context) {
	id, _ := strconv.Atoi(ctx.Param("id"))
	code = model.DeleteCategory(id)
	ctx.JSON(http.StatusOK, gin.H{
		"status":  code,
		"message": errmsg.GetErrMsg(code),
	})
}
