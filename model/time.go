package model

import (
	"database/sql/driver"
	"fmt"
	"time"
)

type LocalTime time.Time

// 虽然该数据类型实际类型为 time.Time，但是不具备 time.Time 的内置⽅法，需要重写 MarshalJSON ⽅法来实现数据解析
func (t *LocalTime) MarshalJSON() ([]byte, error) {
	tTime := time.Time(*t)
	return []byte(fmt.Sprintf("\"%v\"", tTime.Format("2006-01-02 15:04:05"))), nil
}

// 注意：GO的格式化时间的规定时间字符串必须为 2006-01-02 15:04:05
// 这是GO的诞⽣时间，不能更改为其他时间
// (这个时间字符串与java的"yyyy-MM-dd HH:mm:ss")同作⽤
type Model struct {
	ID        int64      `json:"id" gorm:"primary_key"`
	CreatedAt *LocalTime `json:"created_at" gorm:"type:datetime";`
	UpdatedAt *LocalTime `json:"updated_at" gorm:"type:datetime";`
	DeletedAt *LocalTime `json:"deleted_at" sql:"index" gorm:"type:datetime";`
}

// Value⽅法即在存储时调⽤，将该⽅法的返回值进⾏存储，该⽅法可以实现数据存储前对数据进⾏相关操作。
func (t LocalTime) Value() (driver.Value, error) {
	var zeroTime time.Time
	tlt := time.Time(t)
	//判断给定时间是否和默认零时间的时间戳相同
	if tlt.UnixNano() == zeroTime.UnixNano() {
		return nil, nil
	}
	return tlt, nil
}

// Scan⽅法可以实现在数据查询出来之前对数据进⾏相关操作。
func (t *LocalTime) Scan(v interface{}) error {
	if value, ok := v.(time.Time); ok {
		*t = LocalTime(value)
		return nil
	}
	return fmt.Errorf("can not convert %v to timestamp", v)
}
