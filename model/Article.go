package model

import (
	"gitee.com/Mr-zhaopei/blog_demo/utils/errmsg"
	code "gitee.com/Mr-zhaopei/blog_demo/utils/errmsg"
	"github.com/jinzhu/gorm"
)

type Article struct {
	Category Category `gorm:"foreignKey:Cid"` //外键引用
	Model
	Title   string `gorm:"type:varchar(100);not null" json:"title"`
	Cid     int    `gorm:"type:int;not null" json:"cid"`
	Desc    string `gorm:"type:varchar(200)" json:"desc"`
	Content string `gorm:"type:longtext" json:"content"`
	Img     string `gorm:"type:varchar(100)" json:"img"`
}

// 新增加文章
func CreateArticle(data *Article) int {
	err := db.Create(&data).Error
	if err != nil {
		return code.ERROR //500
	}
	return code.SUCCESS
}

// 查询所有文章里列表
func GetArticles(title string, pagesize int, pageNum int) ([]Article, int, int) {
	var articleList []Article
	var total int
	if title == "" {
		// err = db.Preload("Category").Find(&articleList).Count(&total).Limit(pagesize).Offset((pageNum - 1) * pagesize).Error
		// 改进对更新过的文章进行时间排序
		err = db.Order("Updated_At DESC").Preload("Category").Find(&articleList).Limit(pagesize).Offset((pageNum - 1) * pagesize).Error
		//单独计数
		db.Model(&articleList).Count(&total)
		if err != nil && err != gorm.ErrRecordNotFound {
			return nil, errmsg.ERROR, 0
		}
		return articleList, errmsg.SUCCESS, total
	}

	// err = db.Preload("Category").Where("title LIKE ?",title + "%").Find(&articleList).Count(&total).Limit(pagesize).Offset((pageNum - 1) * pagesize).Error
	err = db.Order("Updated_At DESC").Preload("Category").Where("title LIKE ?", title+"%").Find(&articleList).Limit(pagesize).Offset((pageNum - 1) * pagesize).Error

	//单独对总数进行计量
	db.Model(&articleList).Where("title LIKE ?", title+"%").Count(&total)
	if err != nil && err != gorm.ErrRecordNotFound {
		return nil, errmsg.ERROR, 0
	}
	return articleList, errmsg.SUCCESS, total
}

// todo 查询分类下的所有文章
func GetCateArt(id int, pagesize int, pageNum int) ([]Article, int, int) {
	var CateArtList []Article
	var total int
	err = db.Preload("Category").Limit(pagesize).Offset((pageNum-1)*pagesize).Where("cid = ?", id).Find(&CateArtList).Count(&total).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		return nil, errmsg.ERROR_CATEGORY_NOT_EXIT, 0
	}
	return CateArtList, errmsg.SUCCESS, total
}

// todo 查询单个文章
func GetArticleInfo(id int) (Article, int) {
	var artinfo Article
	err = db.Preload("Category").Where("id = ?", id).First(&artinfo).Error
	if err != nil {
		return artinfo, errmsg.ERROR_ARITCLE_NOT_EXIT
	}
	return artinfo, errmsg.SUCCESS
}

// 编辑文章
func EditArticle(id int, data *Article) int {
	var article Article
	var maps = make(map[string]interface{})
	maps["title"] = data.Title
	maps["cid"] = data.Cid
	maps["desc"] = data.Desc
	maps["content"] = data.Content
	maps["img"] = data.Img
	err = db.Model(&article).Where("id = ?", id).Updates(maps).Error
	if err != nil {
		return code.ERROR //500
	}
	return code.SUCCESS
}

// 删除文章
func DeleteArticle(id int) int {
	var article Article
	err = db.Where("id = ?", id).Delete(&article).Error
	if err != nil {
		return errmsg.ERROR
	}
	return errmsg.SUCCESS
}
