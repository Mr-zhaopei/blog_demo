package model

import (
	"gitee.com/Mr-zhaopei/blog_demo/utils/errmsg"
	code "gitee.com/Mr-zhaopei/blog_demo/utils/errmsg"
	"github.com/jinzhu/gorm"
)

type Category struct {
	Model
	// ID   uint   `gorm:"primary_key";auto_increment" json:"id"`
	Name string `gorm:"type:varchar(20);not null" json:"name"`
}

// 查询分类是否存在
func CheckCategory(name string) int {
	var category Category
	db.Select("id").Where("name = ?", name).First(&category)
	if category.ID > 0 {
		return errmsg.ERROR_CATEGORYNAME_USED
	}
	return errmsg.SUCCESS
}

// 新增加分类
func CreateCategory(data *Category) int {
	err := db.Create(&data).Error
	if err != nil {
		return code.ERROR //500
	}
	return code.SUCCESS
}

// 查询单个分类信息
func GetCateInfo(id int) (Category, int) {
	var cate Category
	db.Where("id = ?", id).First(&cate)
	return cate, errmsg.SUCCESS
}

// 查询分类列表
func GetCategorys(pagesize int, pageNum int) ([]Category, int) {
	var categorys []Category
	var total int
	err = db.Limit(pagesize).Find(&categorys).Count(&total).Offset((pageNum - 1) * pagesize).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		return nil, 0
	}
	return categorys, total
}

// 编辑分类
func EditCategory(id int, data *Category) int {
	var category Category
	var maps = make(map[string]interface{})
	maps["name"] = data.Name
	err = db.Model(&category).Where("id = ?", id).Updates(maps).Error
	if err != nil {
		return code.ERROR //500
	}
	return code.SUCCESS
}

// 删除分类
func DeleteCategory(id int) int {
	var category Category
	err = db.Where("id = ?", id).Delete(&category).Error
	if err != nil {
		return errmsg.ERROR
	}
	return errmsg.SUCCESS
}
