package model

import (
	"gitee.com/Mr-zhaopei/blog_demo/utils"
	"gitee.com/Mr-zhaopei/blog_demo/utils/errmsg"
	code "gitee.com/Mr-zhaopei/blog_demo/utils/errmsg"
	"github.com/jinzhu/gorm"
)

type User struct {
	// gorm.Model
	Model
	Username string `gorm:"type:varchar(20);not null" json:"username" validate:"required,min=4,max=12" label:"用户名"`
	Password string `gorm:"type:varchar(100);not null" json:"password" validate:"required,min=6,max=20" label:"密码"`
	Role     int    `gorm:"type:int;DEFAULT:2" validate:"required" json:"role" label:"角色码"` //1的话是管理员，2是阅读者
}

// 查询用户是否存在
func CheckUser(name string) int {
	var users User
	db.Select("id").Where("username = ?", name).First(&users)
	if users.ID > 0 {
		return errmsg.ERROR_USERNAME_USED
	}
	return errmsg.SUCCESS
}

// 更新查询
func CheckUpUser(id int, name string) int {
	var user User
	db.Select("id,username").Where("username = ?", name).First(&user)
	if user.ID == int64(id) {
		return errmsg.SUCCESS
	}
	if user.ID > 0 {
		return errmsg.ERROR_USERNAME_USED
	}
	return errmsg.SUCCESS
}

// 新增加用户
func CreateUser(data *User) int {
	data.Password, _ = utils.EncryptPassword(data.Password)
	err := db.Create(&data).Error
	if err != nil {
		return code.ERROR //500
	}
	return code.SUCCESS
}

// 查询单个用户
func GetUser(id int) (User, int) {
	var user User
	err = db.Where("ID = ?", id).First(&user).Error
	if user.ID > 0 {
		return user, errmsg.ERROR
	}
	return user, errmsg.SUCCESS
}

// 查询用户列表
func GetUsers(username string, pagesize int, pageNum int) ([]User, int) {
	// var user User
	var users []User
	var total int
	//count必须在分页之前，否则报错
	// db.Model(&user).Count(&total)

	if username == "" {
		//直接查找不带userName的参数的用户列表
		db.Select("id,username,role,created_at").Find(&users).Count(&total).Limit(pagesize).Offset((pageNum - 1) * pagesize)
		return users, total
	}
	db.Select("id,username,role,created_at").Find(&users).Where("username LIKE ?", username+"%").Count(&total).Limit(pagesize).Offset((pageNum - 1) * pagesize)
	//改进
	// err = db.Table("user").Count(&total).Limit(pagesize).Offset((pageNum - 1) * pagesize).Find(&users).Error
	if err == gorm.ErrRecordNotFound {
		return nil, 0
	}
	return users, total
}

// 编辑用户
func EditUser(id int, data *User) int {
	var user User
	var maps = make(map[string]interface{})
	maps["username"] = data.Username
	maps["role"] = data.Role
	err = db.Model(&user).Where("id = ?", id).Updates(maps).Error
	if err != nil {
		return code.ERROR //500
	}
	return code.SUCCESS
}

// ChangePassword 修改密码
func ChangePassword(id int, data *User) int {
	var user User
	// var maps = make(map[string]interface{})
	// maps["password"] = data.Password
	data.Password, _ = utils.EncryptPassword(data.Password)
	err = db.Model(&user).Where("id = ?", id).Update("password", data.Password).Error
	if err != nil {
		return errmsg.ERROR
	}
	return errmsg.SUCCESS
}

// 删除用户
func DeleteUser(id int) int {
	var user User
	err = db.Where("id = ?", id).Delete(&user).Error
	if err != nil {
		return errmsg.ERROR
	}
	return errmsg.SUCCESS
}

// 登入验证
func CheckLogin(username string, password string) int {
	var user User

	db.Where("username = ?", username).First(&user)

	//判断用户是否存在
	if user.ID == 0 {
		return errmsg.ERROR_USER_NOT_EXIT
	}

	//判断密码是否正确
	if !utils.EqualsPassword(password, user.Password) {
		return errmsg.ERROR_PASSWORD_WRONG
	}

	//判断用户是否有权限登入
	if user.Role != 1 {
		return errmsg.ERROR_USER_NOT_RIGHT
	}

	return errmsg.SUCCESS

}
