package model

import (
	"fmt"
	"log"
	"time"

	"gitee.com/Mr-zhaopei/blog_demo/config"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
)

//链接数据库

var (
	db    *gorm.DB
	err   error
	mysql config.Mysql
)

func InitDb() {
	data := mysql.Load("conf/db.ini").Init()
	log.Println(data)
	// dsn := "user:pass@tcp(127.0.0.1:3306)/dbname?charset=utf8mb4&parseTime=True&loc=Local"
	dsn := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8mb4&parseTime=True&loc=Local", data.Username,
		data.Password, data.Host, data.Port, data.Database,
	)
	db, err = gorm.Open("mysql", dsn)
	if err != nil {
		fmt.Println("链接数据库失败，请检查参数", err)
	}

	//设置创建数据库的时候不是复数
	db.SingularTable(true)

	// db.AutoMigrate(&User{}, &Category{}, &Article{})
	db.Set("gorm:table_options","ENGINE=InnoDB DEFAULT CHARSET=utf8").AutoMigrate(&User{}, &Category{}, &Article{})

	// SetMaxIdleConns 设置空闲连接池中连接的最大数量
	db.DB().SetMaxIdleConns(10)

	// SetMaxOpenConns 设置打开数据库连接的最大数量。
	db.DB().SetMaxOpenConns(100)

	// SetConnMaxLifetime 设置了连接可复用的最大时间。
	db.DB().SetConnMaxLifetime(10 * time.Second)

	// db.Close()
}
