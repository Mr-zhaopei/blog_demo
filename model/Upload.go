package model

import (
	"context"
	"log"
	"mime/multipart"
	"strconv"

	"gitee.com/Mr-zhaopei/blog_demo/config"
	"gitee.com/Mr-zhaopei/blog_demo/utils/errmsg"
	"github.com/qiniu/go-sdk/v7/auth/qbox"
	"github.com/qiniu/go-sdk/v7/storage"
)

func UploadFile(file multipart.File, fileSize int64) (string, int) {
	var storage_cfg config.Server
	data := storage_cfg.Load("conf/app.ini").Init()
	log.Println(data)

	//七牛云上传
	putPolicy := storage.PutPolicy{
		Scope: data.Bucket,
	}
	mac := qbox.NewMac(data.AccessKey, data.Secretkey)
	upToken := putPolicy.UploadToken(mac)

	zone, _ := strconv.Atoi(data.Zone)
	cfg := storage.Config{
		Zone:          selectZone(zone),
		UseCdnDomains: false,
		UseHTTPS:      false,
	}

	putExtra := storage.PutExtra{}
	formUploader := storage.NewFormUploader(&cfg)
	ret := storage.PutRet{}
	err := formUploader.PutWithoutKey(context.Background(), &ret, upToken, file, fileSize, &putExtra)
	if err != nil {
		return "", errmsg.ERROR
	}
	url := data.QiniuServer + ret.Key
	return url, errmsg.SUCCESS
}

func selectZone(id int) *storage.Zone {
	switch id {
	case 1:
		return &storage.ZoneHuadong
	case 2:
		return &storage.ZoneHuabei
	case 3:
		return &storage.ZoneHuanan
	default:
		return &storage.ZoneHuadong
	}
}
