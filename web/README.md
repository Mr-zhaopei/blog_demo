前端编写
1.引入插件(在bable.config.js文件中添加-cnpm install ant-design-vue@1.7.2)
"plugins": [
    ["import", { "libraryName": "ant-design-vue", "libraryDirectory": "es", "style": "css" }] // `style: true` for less
]
2.引入ant-design-vue
在plugin下创建文件antui.js

3. .eslintrc.js文件编写
```js 
module.exports = {
  root: true,
  env: {
    node: true
  },
  extends: [
    'plugin:vue/essential',
    // '@vue/standard'
  ],
  parserOptions: {
    parser: '@babel/eslint-parser',
    "requireConfigFile": false
  },
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',  
    'vue/multi-word-component-names': "off",
    "vue/no-parsing-error": [2, {
      "x-invalid-end-tag": true,
    }],
  }
}
```
4.打包
